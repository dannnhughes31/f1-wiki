import {useCookie, useFetch, useRuntimeConfig} from "nuxt/app";

export async function retrieve(url, options)
{
    /**
     * Generate a new key for each request, otherwise Nuxt will try to use a cached response.
     */
    let key = (Math.random() + 1).toString(36).substring(7);

    const { data, pending, error, refresh } = await useFetch(url, {

        baseURL: useRuntimeConfig().public.baseUrl,
        initialCache: false,

        credentials: 'include',

        key: key,

        headers: new Headers({
            'X-XSRF-TOKEN': useCookie('XSRF-TOKEN').value,
            'accept': 'application/json',
        }),
        onResponse ({ response }) {
            return response._data
        },
        onResponseError ({ response }) {
            return response._data
        }
    });

    return data.value;
}