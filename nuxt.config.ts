// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ['~/assets/css/main.css'],
  buildModules: [
      '@nuxtjs/tailwindcss',
      '@nuxtjs/google-fonts'
  ],
  modules: ['@nuxtjs/google-fonts'],
  googleFonts: {
    families: {
      'Racing+Sans+One': [400]
    },
    display: 'swap'
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  publicRuntimeConfig: {
    baseURL: process.env.NODE_ENV === 'production' ? process.env.BASE_URL_LIVE : process.env.BASE_URL_LOCAL
  }
})
