/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./app.vue"
  ],  theme: {
    extend: {
      colors: {
        'candy-red': '#FF1801',
        'bronze-normal': '#CD7F32',
        'bronze-light': '#e7cfae',
        'mclaren-orange': '#FF8000',
        'alpine-light-blue': '#2173B8',
        'aston-green': '#037A68',
        'aston-gray': '#666769',
        'alpha-gray': '#20394C',
        'alfa-red': '#E20226',
        'haas-red': '#ed1a3b',
        'williams-blue': '#00A0DE',
        'ferrari-red': '#ff2800'
      }
    },
  },
  plugins: [],
}

